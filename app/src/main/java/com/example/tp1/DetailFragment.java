package com.example.tp1;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp1.data.Country;

public class DetailFragment extends Fragment {

    TextView pays;//on crée les objet vue que l'on vas trouve et remplire
    ImageView drapeau;
    TextView Vcapital;
    TextView Vlangue;
    TextView Vmonaie;
    TextView Vpopulation;
    TextView Vsuperficie;


    RecyclerView recyclerView;//on crée notre recycler de vue ?
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    private Country c;//on crée un objet de type contrie
    private Country[] tabCountry = c.countries;//on fait un tableau de c'est pays en apelant la fonction pour le remplire


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        pays =view.findViewById(R.id.pays);// on trouve les view de notre layout
        Vlangue =view.findViewById(R.id.Vlangue);
        Vmonaie =view.findViewById(R.id.Vmonaie);
        Vpopulation =view.findViewById(R.id.Vpopulation);
        Vsuperficie =view.findViewById(R.id.Vsuperficie);
        Vcapital =view.findViewById(R.id.Vcapital);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());// on recuper l'arg soit icile pays sur le quelel on a clique
        pays.setText(tabCountry[args.getCountryId()].getName());// on remplie les elemnt de la vue
        Vcapital.setText(tabCountry[args.getCountryId()].getCapital());
        Vlangue.setText(tabCountry[args.getCountryId()].getLanguage());
        Vmonaie.setText(tabCountry[args.getCountryId()].getCurrency());
        Vpopulation.setText(Integer.toString(tabCountry[args.getCountryId()].getPopulation()));
        Vsuperficie.setText(Integer.toString(tabCountry[args.getCountryId()].getArea()));

        drapeau =view.findViewById(R.id.drapeau);
        String uri = tabCountry[args.getCountryId()].getImgUri();
        Context c = drapeau.getContext();
        drapeau.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri , null , c.getPackageName())));

        view.findViewById(R.id.retour).setOnClickListener(new View.OnClickListener() {// on met en place le boutton de retout sur le premier fragment/vue
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}