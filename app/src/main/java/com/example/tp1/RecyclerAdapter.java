package com.example.tp1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tp1.data.Country;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>  {

    RecyclerView recyclerView;//on crée notre recycler de vue ?
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    private Country c;//on crée un objet de type contrie
    private Country[] tabCountry = c.countries; //on fait un tableau de c'est pays en apelant la fonction pour le remplire





    //@Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {// je sais pas
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    //@Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {//on definie les valuer des morceau de la vue avec notre objet contrie

        viewHolder.pays.setText(tabCountry[i].getName());
        viewHolder.capitale.setText(tabCountry[i].getCapital());
        String uri = tabCountry[i].getImgUri();//image
        Context c = viewHolder.drapeau.getContext();
        viewHolder.drapeau.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri , null , c.getPackageName())));


    }

    @Override
    public int getItemCount()
    {
        return tabCountry.length;
    }



    public class ViewHolder extends RecyclerView.ViewHolder{//on definie la classe viewHolder qui contiendra pour attribut les element de du notre layout
        private TextView pays;
        private TextView capitale;
        private ImageView drapeau;

        ViewHolder(View itemView)//le contruteur definie les atribut par les element du layout rée
        {
            super(itemView);
            pays =(TextView)itemView.findViewById(R.id.pays);
            capitale =(TextView)itemView.findViewById(R.id.capital);
            drapeau =(ImageView)itemView.findViewById(R.id.drapeau);

            itemView.setOnClickListener(new View.OnClickListener() {//on definie l'action de click que l'element de notre vue pour acceder a la second vue
                @Override public void onClick(View v) {
                    int position = getAdapterPosition();

                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });
        }

    }

}


